package br.ucsal.bes20201.testequalidade.locadora.dominio.enums;

public enum SituacaoVeiculoEnum {
	DISPONIVEL, MANUTENCAO, LOCADO;
}
